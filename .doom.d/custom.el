(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files
   '("~/Documents/Estudios/org-notes/Apuntes USACH/Algebra 2/Teoria.org" "~/Documents/Estudios/org-notes/Apuntes USACH/Calculo 2/Teoria.org" "~/Documents/Estudios/org-notes/Apuntes USACH/Fisica 2/Fisica II Ejercicio.org" "~/Documents/Estudios/org-notes/Apuntes USACH/Fisica 2/Teoria.org" "~/Documents/Estudios/org-notes/Apuntes USACH/Fundamentos Programación/Teoria.org" "~/Documents/Estudios/org-notes/Apuntes USACH/Fundamentos de Computación/FDC.org" "~/Documents/Estudios/org-notes/Apuntes USACH/Introducción a la Ing.Informática/3I.org" "~/Documents/Estudios/org-notes/Daily/19-03-2021.org" "~/Documents/Estudios/org-notes/Daily/2021-03-21.org" "~/Documents/Estudios/org-notes/Random-Notes/testing.org" "~/Documents/Estudios/org-notes/journal/2021-03-17.org" "~/Documents/Estudios/org-notes/journal/journal.org" "~/Documents/Estudios/org-notes/CoolAscii.org" "~/Documents/Estudios/org-notes/Music.org" "~/Documents/Estudios/org-notes/gtd.org" "~/Documents/Estudios/org-notes/journal.org" "~/Documents/Estudios/org-notes/notes.org" "~/Documents/Estudios/org-notes/todo.org" "/home/hakuya/Documents/Estudios/org-notes/Daily/19-04-2021.org"))
 '(org-journal-date-format "%A, %d %B %Y")
 '(org-journal-date-prefix "#+TITLE: ")
 '(org-journal-dir "~/Documents/Estudios/org-notes/Daily/")
 '(org-journal-file-format "%d-%m-%Y.org")
 '(org-startup-folded t)
 '(package-selected-packages '(org-roam-bibtex org-wild-notifier auctex)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((((type tty))))))
